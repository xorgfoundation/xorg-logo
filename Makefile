.SUFFIXES: .source.svg .svg

SOURCE_SVG = \
	x-org-foundation-rect.source.svg \
	x-org-foundation-square.source.svg \
	x-org.source.svg

.source.svg.svg:
	inkscape -T -l -o $@ $*.source.svg 2>/dev/null

PATH_SVG=$(SOURCE_SVG:.source.svg=.svg)

all: $(PATH_SVG)

clean:
	rm -f $(PATH_SVG)
