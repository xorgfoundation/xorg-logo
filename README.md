# X.org logos in SVG form

A variety of logos to choose from

 * X Classic. Just the X, no fancy bits. x-logo.svg

 * With stylish ring. x-org-plain.svg

 * With ring and '.Org'. x-org.svg

 * With ring, '.Org' and 'Foundation' in rectagular
   form. x-org-rect.svg

 * With ring, '.Org' and 'Foundation in square form. x-org-square.svg

For those logos with text, there are also versions of each with the
text as text elements instead of paths. Use these when you need to
edit them, regenerating the path-only versions when you're done.
